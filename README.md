# Plan 9 Scripts

A collection of my rc scripts for plan 9 to make my life easier.

## Starting Scripts:

 * `acmestart`
 * `barstart`
 * `chatstart`
 * `hacme`
 * `hterm`
 * `infstart`
 * `riostart`
 * `uacme`
 * `uterm`
 
These scripts all start some other program with some predefined parameters that
make it more convenient to just run the script.
`acmestart`, `barstart`, `infstart`, and `riostart` all start their respective
programs, `acmestart` has different fonts active, `barstart` changes the theme
of the bar `infstart` starts inferno, `riostart` starts rio.

`hacme` and `uacme` allow me to access my host and virtualised Ubuntu file-system
in `acme`. `hterm` and `uterm` allow me to ssh in a `vt` terminal into these same
two operating systems.
 
## Utility Scripts:
 * `fixdrawterm`
 * `infman`
 * `sharefs`
 
`fixdrawterm` fixes any `drawterm` window with a background wallpaper, usually
they artifact, but with `fixdrawterm` the window is resized twice to ensure
that the artifacts stop.
 
`infman` allows one to access any inferno manual page from plan 9.

`sharefs` shares the filesystem over the network for any other plan 9 computer
to connect to.

<!--  LocalWords:  virtualised
 -->
